package ImageProcessing;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFColor;



public class ImageProcessing {

	static File imageFile = null;
	static String DirectoryAddress = "C:/Users/SaN/workspace1/Demo/src/main/java/ImageProcessing/";
	static String imageName = "test.png";
	static String xlFileName = "Test - Copy.xlsx";
	static String imageAddress = null;
	static String xlFileAddress = null;
	static int imageHeight = 0;
	static int imageWidth = 0;
	static Xls_Reader xlReader = null;
	static Workbook wb = null;
	static Sheet sheet  = null;
	static String imageSheetName = "Image";
	static BufferedImage image = null;
	
	

	public ImageProcessing() {
		imageAddress = DirectoryAddress+imageName;
		xlFileAddress = DirectoryAddress+xlFileName;
		imageFile= new File(imageAddress);
		xlReader = new Xls_Reader(xlFileAddress);
		ImageProcessing.wb= Xls_Reader.workbook;
		ImageProcessing.sheet = wb.getSheet(imageSheetName);
		
		try {
			image = ImageIO.read(imageFile);
			imageHeight = image.getHeight();
			imageWidth = image.getWidth();
			System.out.println("Image dimension is "+imageHeight +" x "+imageWidth);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static XSSFColor getImageRGBbyPixel(int xPixelCoordinate, int yPixelCoordinate) {
		// Getting pixel color by position x and y 
		int clr=  image.getRGB(xPixelCoordinate,yPixelCoordinate); 
		int  red   = (clr & 0x00ff0000) >> 16;
		int  green = (clr & 0x0000ff00) >> 8;
		int  blue  =   (clr & 0x000000ff);
		String hex = String.format("%02X%02X%02X", red, green, blue); 
	    

		
		XSSFColor color = new XSSFColor();
		color.setARGBHex(hex);
		
		return color;
		
	}

	public static String printPixelARGB(int pixel) {
	    int alpha = (pixel >> 24) & 0xff;
	    int red = (pixel >> 16) & 0xff;
	    int green = (pixel >> 8) & 0xff;
	    int blue = (pixel) & 0xff;
	    System.out.println("argb: " + alpha + ", " + red + ", " + green + ", " + blue);
	    String hex = String.format("%02X%02X%02X", red, green, blue); 
	    System.out.println(hex);
	    return hex;
	  }

	
		public static void main(String[] args) {

		ImageProcessing  imp = new ImageProcessing();
		for(int rowNum = 0; rowNum<imageHeight-1; rowNum++) {
			System.out.println(rowNum);
			Row row  = sheet.createRow(rowNum);
			for (int colNum = 0; colNum<imageWidth-1; colNum++) {
				xlReader.highlightCell(row.createCell(colNum),getImageRGBbyPixel(colNum,rowNum));
			}
		}
		
		
		getImageRGBbyPixel(1,1);
		xlReader.writeFile(xlFileAddress);
		
		
		




	}

}
